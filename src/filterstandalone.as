package
{
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.*;
	import flash.media.Sound;
	import flash.net.*;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	
	// Demonstrates very basic interacte dynamic sound: setting 
	// pan and gain in response to mouse movement.
	//note: use 192kbps mp3, below 320kbps, if possible to "hide" the static
	[SWF(width='800',height='600',backgroundColor='0x777777')]
	public class filterstandalone extends Sprite
	{
		private const LOG_N:uint = 10;          // Log2 of the FFT length (2048)
		private const N:uint = 1 << LOG_N;        // FFT Length (2^LOG_N)
		
		private var u:uint;
		private var txtfld:TextField;
		
		private const SAMPLE_RATE:Number = 44100;
		
		private const CHUNK_LEN:uint = 2048;	// [2048, 8192]
		private var m_bufL:Vector.<Number>;		// Left buffer
		private var m_bufR:Vector.<Number>;		// Right buffer
		
		private var m_preFiltL:Vector.<Number>;	// Audio before filtering 
		private var m_preFiltR:Vector.<Number>;
		private var m_postFiltL:Vector.<Number>;	// Audio after filtering
		private var m_postFiltR:Vector.<Number>;
		
		
		private var m_reL:Vector.<Number>;		// real and imaginary values for FFT
		private var m_reR:Vector.<Number>;
		private var m_imL:Vector.<Number>;
		private var m_imR:Vector.<Number>;
		
		private var reaL:Vector.<Number>;
		private var reaR:Vector.<Number>;
		private var imgL:Vector.<Number>;
		private var imgR:Vector.<Number>;
		
		private const triBandL:EQ = new EQ();
		private const triBandR:EQ = new EQ();
		
		private var matPar:Vector.<Number>;	//material parameters
		private var matDiv:Vector.<Number>;
		
		private var m_pan:Number = 0.0;
		
		private var m_filter:OpBiquad;			// Biquad filter
		
		//--------------------------
		private var m_mp3:Sound;			
		private var m_outSnd:Sound;
		//--------------------------
		
		private const BOX_WIDTH:Number = 600;
		private const BOX_HEIGHT:Number = 400;
		
		// Constructor. Sets up the audio buffers and output.
		public function filterstandalone()
		{
			//debugging "console"
			txtfld = new TextField();
			txtfld.x = 0;
			txtfld.y = 0;
			txtfld.width = 500;
			stage.addChild(txtfld);
			
			
			// Create a red rectangle, listen for mouse moves
			var box:Sprite = new Sprite
			box.x = 100;
			box.y = 100;
			box.graphics.beginFill(0x7F0000);
			box.graphics.drawRect(0, 0, BOX_WIDTH, BOX_HEIGHT);
			box.graphics.endFill();
			this.addChild(box);
			box.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);	
			
			// Create buffer for a chunk of audio
			m_bufL = new Vector.<Number>(CHUNK_LEN);	
			m_bufR = new Vector.<Number>(CHUNK_LEN);	
			
			m_preFiltL = new Vector.<Number>(CHUNK_LEN);
			m_preFiltR = new Vector.<Number>(CHUNK_LEN);
			m_postFiltL = new Vector.<Number>(CHUNK_LEN);
			m_postFiltR = new Vector.<Number>(CHUNK_LEN);
			
			m_reL=new Vector.<Number>(N);		// real and imaginary values for FFT
			m_reR=new Vector.<Number>(N);
			m_imL=new Vector.<Number>(N);
			m_imR=new Vector.<Number>(N);
			
			reaL=new Vector.<Number>(N);
			reaR=new Vector.<Number>(N);
			imgL=new Vector.<Number>(N);
			imgR=new Vector.<Number>(N);
			
				//triBandL = new EQ();
				//triBandR = new EQ();
			
			matPar= new Vector.<Number>();
			matDiv = new Vector.<Number>();
			/*
			matPar.push(0.02);
			matPar.push(0.28);
			matPar.push(3);//0.97
			matPar.push(3);//0.96
			matPar.push(3);//0.95
			matPar.push(3);//0.97
			
			matDiv.push((1-0.42)/3);
			matDiv.push((0.78-0.42)/2);
			matDiv.push((3-0.78)/5);
			matDiv.push((0)/10);
			matDiv.push((0)/22);
			matDiv.push((0)/46);
			*/
			
			// Create filter
			m_filter = new OpBiquad(SAMPLE_RATE);
			m_filter.setBandpass(1000);
			
			// Initialize the mp3 sound source
			m_mp3 = new Sound(new URLRequest("battle.mp3"));
			m_mp3.addEventListener(Event.COMPLETE, onLoadComplete);
			
			
			// Set up the Sound object
			m_outSnd = new Sound;
			m_outSnd.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleDataEvent);
			//m_outSnd.play();
			
		}
		
		// Called when mp3 finishes loading
		private function onLoadComplete(event:Event):void
		{
			m_outSnd.play();
		}
		
		// SampleDataEvent handler.
		private function onSampleDataEvent(event:SampleDataEvent):void
		{
			generateAudio(m_bufL, m_bufR, CHUNK_LEN);
			for (var i:uint = 0; i < CHUNK_LEN; i++)
			{
				event.data.writeFloat(m_bufL[i]);
				event.data.writeFloat(m_bufR[i]);
			}
		}
		
		//FFT equalizer
		private function doorTransmit(tempEq:EQ):void
		{
			tempEq.lllg *= 1;
			tempEq.llg *= 0.494;
			tempEq.lg *= 0.081;
			tempEq.mg *= 0;
			tempEq.hg *= 0;
			tempEq.hhg *= 0;
			tempEq.hhhg *= 0.046;
		}
		
		private function doorReflect(tempEq:EQ):void
		{
			//tempSound.setHighpass(600);
			tempEq.lllg *= 0;
			tempEq.llg *= 0.08;
			tempEq.lg *= 0.253;
			tempEq.mg *= 0.517;
			tempEq.hg *= 0.839;
			tempEq.hhg *= 1;
			tempEq.hhhg *= 1;
		}
		
		private function concreteReflect(tempEq:EQ):void
		{
			tempEq.lllg *= 0.494;
			tempEq.llg *= 0.425;
			tempEq.lg *= 0.379;
			tempEq.mg *= 0.471;
			tempEq.hg *= 0.402;
			tempEq.hhg *= 0.310;
			tempEq.hhhg *= 0.379;
		}
		private function concreteTransmit(tempEq:EQ):void
		{
			tempEq.lllg *= 0.126;
			tempEq.llg *= 0.149;
			tempEq.lg *= 0.218;
			tempEq.mg *= 0.103;
			tempEq.hg *= 0.103;
			tempEq.hhg *= 0.195;
			tempEq.hhhg *= 0.080;
		}
		
		private function glassReflect(tempEq:EQ):void
		{
			tempEq.lllg *= 0;
			tempEq.llg *= 0.046;
			tempEq.lg *= 0.103;
			tempEq.mg *= 0.195;
			tempEq.hg *= 0.425;
			tempEq.hhg *= 0.770;
			tempEq.hhhg *= 1;
		}
		private function glassTransmit(tempEq:EQ):void
		{
			tempEq.lllg *= 0.609;
			tempEq.llg *= 0.356;
			tempEq.lg *= 0.264;
			tempEq.mg *= 0.172;
			tempEq.hg *= 0.126;
			tempEq.hhg *= 0.092;
			tempEq.hhhg *= 0.069;
		}
		
		// Generate audio data
		private function generateAudio(
			outL:Vector.<Number>,
			outR:Vector.<Number>,
			n:uint):void
		{
			var i:uint;
			
			var byteArray:ByteArray = new ByteArray;
			
			// Try to grab n samples from the mp3
			var len1:uint = m_mp3.extract(byteArray, n);
			
			// If we couldn't get all n, go to beginning of file
			var len2:uint = n - len1;
			if (len2 > 0)
				len2 = m_mp3.extract(byteArray, len2, 0);
			
			// Rewind the byte array
			byteArray.position = 0;
			
			//change these values to change sound quality
			triBandL.lllg *= 0.2;
			triBandL.llg *= 0.2;
			triBandL.lg *= 0.2;
			triBandL.mg *= 1;
			triBandL.hg *= 0.2;
			triBandL.hhg *= 0.2;
			triBandL.hhhg *= 0.2;
			triBandR.lllg *= 0.2;
			triBandR.llg *= 0.2;
			triBandR.lg *= 0.2;
			triBandR.mg *= 1;
			triBandR.hg *= 0.2;
			triBandR.hhg *= 0.2;
			triBandR.hhhg *= 0.2;
			for (i = 0; i < n; i++)
			{
				//m_preFiltL[i] = byteArray.readFloat();
				//m_preFiltR[i] = byteArray.readFloat();
				
				m_reL[i] = triBandL.compute(byteArray.readFloat());
				m_reR[i] = triBandR.compute(byteArray.readFloat());
				m_imL[i] = 0;
				m_imR[i] = 0;
			}
			
			/*// Filter it
			var fft:FFT2 = new FFT2;
			fft.init(LOG_N);
			
			fft.run(m_reL, m_imL, FFT2.FORWARD);
			fft.run(m_reR, m_imR, FFT2.FORWARD);
			
//			//TODO post-FFT ((re*2)/N) and ((im*2)/N) -- if ifft is shit
			doorReflect(m_reL, m_reR, m_imL, m_imR);

			fft.run(m_reL, m_imL, FFT2.INVERSE);
			fft.run(m_reR, m_imR, FFT2.INVERSE);
*/
			//m_filter.process(m_preFiltL, m_postFiltL, n);
			//m_filter.process(m_preFiltR, m_postFiltR, n);
			//doorTransmit(m_filter);
			
			//m_filter.process(m_reL, reaL, n);
			//m_filter.process(m_reR, reaR, n);
			
			// Convert gain and pan into gain for each channel
			var gainL:Number = 2*Math.min(1, 1-m_pan);
			var gainR:Number = 2*Math.max(0, 1+m_pan);
			
			// Copy the audio data into the output buffer
			for (i = 0; i < n; i++)
			{
				//m_bufL[i] = gainL*m_postFiltL[i];
				//m_bufR[i] = gainR*m_postFiltR[i];
				m_bufL[i] = gainL*m_reL[i];
				m_bufR[i] = gainR*m_reR[i];
			}
			
		}
		
		// Called in response to mouse movement
		private function onMouseMove(event:MouseEvent):void
		{
			// Set cutoff frequency depending on vertical position of mouse
			const MIN_CUTOFF:Number = 100;	
			const MAX_CUTOFF:Number = 15000;
			var cutoff:Number = MIN_CUTOFF * Math.pow(MAX_CUTOFF/MIN_CUTOFF, event.localY/BOX_HEIGHT);
			
			
			//m_filter.setHighpass(1400);
			//m_filter.setLowpass(cutoff);
			//m_filter.setBandpass(cutoff);
			//trace(cutoff);//only in debug mode
			txtfld.text = MIN_CUTOFF + "*" + (MAX_CUTOFF/MIN_CUTOFF) + "^" + event.localY/BOX_HEIGHT + "=" + cutoff.toString();
			// Pan is zero in center, 1 on right, -1 on left.
			//m_pan  = (event.localX - 0.5*BOX_WIDTH) / (0.5*BOX_WIDTH);
		}
	}
}

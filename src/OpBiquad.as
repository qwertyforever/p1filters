package
{
	/**
	 * Implementation of a biquad filter, that is, a filter
	 * with 2 poles and 2 zeroes.
	 * 
	 * The biquad topology is very flexible in that it
	 * allows you to place a conjugate pair of poles and
	 * a conjugate pair of zeros pretty much anywhere on
	 * the z-plane. (For stability, the poles should be 
	 * within the unit circle, of course).
	 * 
	 * The topology is the so-called "Direct Form II".  Different
	 * authors have different conventions for the names of the
	 * coefficients and even their signs.  I'm using the conventions
	 * from Julius O. Smith:
	 * 
	 * 	http://ccrma-www.stanford.edu/~jos/filters/Direct_Form_II.html
	 * 
	 * x ---->o--------->o--- b0 -->o-----> y
	 *        ^          |vd0        ^
	 *        |          |          |
	 *        |        z^-1         |
	 *        |          |          |
	 *        |          v          |
	 *        o<-- -a1 --o--- b1 -->o
	 *        ^          |vd1        ^
	 *        |          |          |
	 *        |        z^-1         |
	 *        |          |          |
	 *        |          v          |
	 *        o<-- -a2 --o--- b2 -->o
	 *                    vd2
	 * 
	 * The difference equation can be written as
	 *		v(n) = x(n) - a1 v(n-1) - a2 v(n-2)
	 *		y(n) = b0 v(n) + b1 v(n-1) - b2 v(n-2)
	 * which can be interpreted as a two-pole filter followed by a two-zero filter.
	 * 
	 * The z-transform works out to be:
	 *		V(z)/X(z) = 1/(1 + a1 z^-1 + a2 ^ z-2)
	 *		Y(z)/V(z) = b0 + b1 z^-1 + b2 z^-2
	 * and therefore:
	 * 
	 *		Y(z)	(b0 + b1 z^-1 + b2 z^-2) 
	 *		---- =	------------------------
	 *  	X(z)	(1 + a1 z^-1 + a2 ^ z-2)
	 * 
	 * Since the numerator and denominator are both quadratic equations, the filter
	 * is known as a "biquad". The pole and zero positions can be computed using the
	 * well-known solution to the quadratic equation...
	 * 		ax^2 + bx + c = 0
	 * ...namely:
	 * 		x = [-b +/- sqrt(b^2 - 4ac)] / 2a
	 * 
	 * The convention for the a and b coefficient are almost the same as those in
	 * "Cookbook formulae for audio EQ biquad filter coefficients" by Robert Bristow-Johnson 
	 *		http://www.musicdsp.org/files/Audio-EQ-Cookbook.txt
	 * which is reproduced in full below (after the code).
	 * The difference is that RBJ has an extra a0 factor... but that's easy to address
	 * by computing a0,a1,a2,b0,b1,b2, and then dividing all coefficients by a0.
	 * 
	 */
	public class OpBiquad
	{
		private var m_sr:Number = 0.0;
		
		private var m_a1:Number = 0.0;	// Coefficients for two-pole filter
		private var m_a2:Number = 0.0;
		private var m_b0:Number = 1.0;	// Coefficients for two-zero filter
		private var m_b1:Number = 0.0;
		private var m_b2:Number = 0.0;

		private var m_vd1:Number = 0.0;	// State variables
		private var m_vd2:Number = 0.0;
		
		/**
		 * Constructor.
		 * @param	sr			Samplerate (optional). Call init if you prefer.
		 * @param	numChannels	Number of channels.
  		 */
		public function OpBiquad( 
			sr:Number)
		{
			// Remember sample rate
			m_sr = sr;
		}
		
		/**
		 * Set up as low-pass filter.
		 * @param	f	Cutoff frequency (probably 3dB point, but I'm not sure -Gerry)
		 * @param	Q	Filter Q. Higher Q means gain has bigger peak just before cutoff.
		 */
		public function setLowpass(
			f0:Number,
			Q:Number = 1.0 ):void
		{
			f0 = Math.min( f0, 0.95*m_sr/2 );
			
			var a0:Number;
			var w0:Number = 2*Math.PI*f0/m_sr;
			
			var alpha:Number = Math.sin(w0)/(2*Q);
			var cosw0:Number = Math.cos(w0);
						
			m_b0 = (1 - cosw0)/2;
			m_b1 =  1 - cosw0;
			m_b2 = (1 - cosw0)/2;

			a0 =  1 + alpha;
			m_a1 = -2*cosw0;
			m_a2 = 1 - alpha;
			
			m_b0 /= a0;
			m_b1 /= a0;
			m_b0 /= a0;
			m_a1 /= a0;
			m_a2 /= a0;
		}
		
		/**
		 * Set up as high-pass filter.
		 * @param	f0	Cutoff frequency (probably 3dB point, but I'm not sure -Gerry)
		 * @param	Q	Filter Q. Higher Q means gain has bigger peak just before cutoff.
		 */
		public function setHighpass(
			f0:Number,
			Q:Number = 1.0 ):void
		{
			f0 = Math.min( f0, 0.95*m_sr/2 );
			
			var a0:Number;
			var w0:Number = 2*Math.PI*f0/m_sr;
			var alpha:Number = Math.sin(w0)/(2*Q);
			var cosw0:Number = Math.cos(w0);
						
            m_b0 =  (1 + cosw0)/2;
            m_b1 = -(1 + cosw0);
            m_b2 =  (1 + cosw0)/2;
            a0 =   1 + alpha;
            m_a1 =  -2*cosw0;
            m_a2 =   1 - alpha;
			
			m_b0 /= a0;
			m_b1 /= a0;
			m_b0 /= a0;
			m_a1 /= a0;
			m_a2 /= a0;
		}
		
		/**
		 * Set up as band-pass filter (with 0 dB peak gain)
		 * @param	f0	Center frequency
		 * @param	Q	Filter Q.  Higher Q means narrower bandwidth.
		 */
		public function setBandpass(
			f0:Number,
			Q:Number = 1.0 ):void
		{
			f0 = Math.min( f0, 0.95*m_sr/2 );

			var a0:Number;
			var w0:Number = 2*Math.PI*f0/m_sr;
			var alpha:Number = Math.sin(w0)/(2*Q);
			var cosw0:Number = Math.cos(w0);
			
			m_b0 =   alpha;
			m_b1 =   0;
			m_b2 =  -alpha;
			a0 =   1 + alpha;
			m_a1 =  -2*cosw0;
			m_a2 =   1 - alpha;			
			
			m_b0 /= a0;
			m_b1 /= a0;
			m_b0 /= a0;
			m_a1 /= a0;
			m_a2 /= a0;
			//clear();
		}

		/**
		 * Filter a vector of samples
		 * @param	x		Input samples
		 * @param	y		Output samples
		 * @param	n		Number of samples
		 */
		public function process(
			x:Vector.<Number>,
			y:Vector.<Number>,
			n:uint):void
		{
			var i:uint;
			var vd0:Number = 0;

			for ( i = 0; i < n; i++ )
			{
				vd0 = x[i] - m_a1*m_vd1 - m_a2*m_vd2;
				y[i] = m_b0*vd0 + m_b1*m_vd1 + m_b2*m_vd2;
				m_vd2 = m_vd1;
				m_vd1 = vd0;
			}
		}

	}
}


